drop database if exists `coolmeeting`;

create database `coolmeeting` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `coolmeeting`;

-- =================员工表===========================

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `username` varchar(20) NOT NULL DEFAULT '',
     `password` varchar(50) NOT NULL DEFAULT '',
     `real_name` varchar(20) NOT NULL DEFAULT '',
     `phone` varchar(20) NOT NULL DEFAULT '',
     `email` varchar(50) NOT NULL DEFAULT '',
     `status` varchar(5) NOT NULL DEFAULT '0' COMMENT '0正常 1未审核 2审核不通过',
     `dept_id` int(11) NOT NULL DEFAULT 0,
     `role` varchar(12) DEFAULT '1' COMMENT '1普通用户 2管理员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;


insert  into `employee`(`id`,`real_name`,`username`,`phone`,`email`,`status`,`dept_id`,`password`,`role`)
values (8,'王晓华','wangxh','13671075406','wang@qq.com','0',13,'1','1'),
       (9,'林耀坤','linyk','13671075406','yang@qq.com','0',13,'1','2'),
       (10,'熊杰文','xiongjw','134555555','xiong@qq.com','0',13,'1','2'),
       (11,'王敏','wangmin','1324554321','wangm@qq.com','0',15,'1','2'),
       (15,'黄美玲','huangml','huangml@qq.com','13567898765','0',15,'1','2'),
       (20,'王敏','wangmin002','13454332334','wang@qq.com','0',15,'1','2'),
       (21,'陈敏','chenm','13559994444','www@aa.com','1',15,'1','2'),
       (23,'陈晨','wangm','22·2','11','0',14,'1','2'),
       (25,'王晓华','wangxh222','111','1','0',14,'1','2'),
       (27,'张三','zhangsan','122','22','1',16,'1','2'),
       (28,'李四','lisi','1','1','0',16,'1','2'),
       (29,'王五','wangwu','1','1','0',17,'1','2'),
       (30,'赵六','zhaoliu','1','1','0',13,'1','2'),
       (32,'冯七','fengqi','1','1','0',13,'1','2'),
       (33,'马八','maba','1','1','0',13,'1','2'),
       (34,'钱十','qianshi','1','1','2',13,'1','2'),
       (35,'周十一','zhoushiyi','1','1','0',13,'1','2'),
       (36,'wushier','wushier','1','1','1',13,'1','2');

-- =================访问统计表===========================

DROP TABLE IF EXISTS `counter`;

CREATE TABLE `counter` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `visit_count` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  COMMENT '访问统计表' AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

insert  into `counter`(`id`, `visit_count`) values (1, 05);

-- =================部门表===========================

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '部门名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 COMMENT '部门表' DEFAULT CHARSET=utf8;

insert into `department`(`id`, `name`)
values (13,'技术部'),
       (14,'财务部'),
       (15,'市场部'),
       (16,'商务部'),
       (17,'销售部'),
       (20,'生产部');


-- =================会议表===========================


DROP TABLE IF EXISTS `meeting`;

CREATE TABLE `meeting` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(20) NOT NULL DEFAULT '' COMMENT '会议名称',
   `room_id` int(11) DEFAULT NULL COMMENT '房间id',
   `reserve_id` int(11) DEFAULT NULL COMMENT '预定人id',
   `number_of_person` int(11) DEFAULT NULL COMMENT '预计参加人数',
   `start_time` datetime DEFAULT NULL COMMENT '开始时间',
   `end_time` datetime DEFAULT NULL COMMENT '结束时间',
   `reserve_time` datetime DEFAULT NULL COMMENT '预定时间',
   `desc` varchar(200) NOT NULL DEFAULT '' COMMENT '会议描述',
   `status` varchar(20) DEFAULT NULL COMMENT '会议状态 0正常 1取消',
   `cancel_time` datetime DEFAULT NULL COMMENT '取消时间',
   `cancel_reason` varchar(255) NOT NULL DEFAULT '' COMMENT '取消原因',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT '会议表' AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;


insert  into `meeting`(`id`,`name`,`room_id`,`reserve_id`,`number_of_person`,`start_time`,`end_time`,`reserve_time`,`cancel_time`,`desc`,`status`,`cancel_reason`)
values (25,'ces',5,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-10 23:02:39','2020-08-23 14:48:40','','0','aaaaaaaaaaaaa'),
       (26,'测测',7,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-17 23:04:18','2020-01-11 01:06:20','','0',''),
       (27,'我看看',6,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-10 23:06:33','2020-01-11 01:01:42','我看看','0',''),
       (28,'运营会',5,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-10 23:26:26','2020-08-23 14:49:06','测试','0','ppppppppppppppp'),
       (29,'市场部会议',6,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-10 23:44:41',NULL,'市场部','0',''),
       (30,'内部会议',10,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-10 23:56:20',NULL,'内部会议','0',''),
       (31,'我的会议',9,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:35:11',NULL,'测试','0',''),
       (32,'我的会议哈哈',5,8,10,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:40:50',NULL,'','0',''),
       (33,'哈哈',6,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:42:09','2020-01-12 11:44:57','你好','0',''),
       (34,'我的会议3',8,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:42:51',NULL,'测试','0',''),
       (35,'我的会议',7,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:44:35',NULL,'','0',''),
       (36,'我问问',7,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:57:56','2020-01-11 16:59:57','地点','0',''),
       (37,'我的会议4',7,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-11 16:59:49',NULL,'我的会议','0',''),
       (38,'班会',9,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-12 11:49:17','2020-01-12 11:49:37','班会','1',''),
       (39,'测试会议',5,8,12,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-01-14 14:44:07',NULL,'ss','0',''),
       (40,'市场研究会议',5,8,99,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-08-16 21:23:45',NULL,'222222222222222222222','0',''),
       (41,'哈哈哈',17,8,99,'2020-08-25 12:00:00','2020-08-30 12:00:00','2020-08-21 14:28:36',NULL,'aaaaaaaaaaaaaaaaaaa','0','');


-- =================会议-员工表===========================


DROP TABLE IF EXISTS `meeting_employee`;

CREATE TABLE `meeting_employee` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `meeting_id` int(11) NOT NULL,
   `employee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 COMMENT '会议-员工表' DEFAULT CHARSET=utf8;



insert  into `meeting_employee`(`id`,`meeting_id`,`employee_id`)
values (1,28,13),
       (2,28,23),
       (3,28,27),
       (4,28,16),
       (5,29,16),
       (6,29,13),
       (7,29,8),
       (8,30,15),
       (9,30,13),
       (10,30,8),
       (11,30,23),
       (12,27,8),
       (13,26,8),
       (14,25,8),
       (15,28,8),
       (16,31,8),
       (17,31,17),
       (18,31,23),
       (19,32,8),
       (20,32,17),
       (21,33,15),
       (22,34,8),
       (23,34,17),
       (24,35,8),
       (25,36,9),
       (26,36,8),
       (27,37,8),
       (28,37,23),
       (29,38,11),
       (30,38,16),
       (31,38,20),
       (32,39,13),
       (33,40,10),
       (34,40,8),
       (35,40,9),
       (36,41,10),
       (37,41,8),
       (38,41,9);

-- =================房间表===========================

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `room_num` int(11) NOT NULL COMMENT'编号',
   `name` varchar(20) NOT NULL DEFAULT '' COMMENT '名称',
   `capacity` int(11) DEFAULT NULL COMMENT'容量',
   `status` varchar(5) DEFAULT '0' COMMENT'状态,0未占用，1已占用',
   `desc` varchar(200) DEFAULT NULL COMMENT'描述',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT '房间表' AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;


insert  into `room`(`id`,`room_num`,`name`,`capacity`,`status`,`desc`)
values (5,101,'第一会议室',15,'0','公共会议室'),
       (6,102,'第二会议室',5,'0','管理部门会议室'),
       (7,103,'第三会议室',12,'0','市场部专用会议室'),
       (8,401,'第四会议室',15,'0','公共会议室'),
       (9,201,'第五会议室',15,'0','最大会议室'),
       (10,601,'第六会议室',12,'0','需要提前三天预定'),
       (15,999,'第998会议室',99,'0','爱笑会议室'),
       (16,998,'第999会议室',98,'1','游戏会议室'),
       (17,996,'第10000会议室',96,'0','狼人杀会议室');
