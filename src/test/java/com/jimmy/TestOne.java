package com.jimmy;

import com.jimmy.pojo.Department;
import com.jimmy.pojo.Employee;
import com.jimmy.pojo.Meeting;
import com.jimmy.pojo.vo.MeetingVo;
import com.jimmy.service.DepartmentService;
import com.jimmy.service.MeetingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class) //声明spring提供的单元测试环境
@ContextConfiguration(locations = "classpath:applicationContext.xml")//指定IoC容器的配置信息
public class TestOne {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private MeetingService meetingService;

    // 测试 Spring与MyBatis整合是否成功
    @Test
    public void test() {
        List<Department> depts = departmentService.getAllDepts();
        for (Department dept : depts) {
            System.out.println(dept);
        }

    }

    @Test
    public void test1() {
        Department dept = departmentService.getDeptByName("市场部");
        System.out.println(dept);

    }

    @Test
    public void test2() {
        MeetingVo meetingVo = meetingService.getMeetingById(28);
        System.out.println(meetingVo);

    }


    @Test
    public void test3() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        MeetingVo meetingVo = new MeetingVo();
        meetingVo.setName("name");
        meetingVo.setStartTime(sdf.parse("2020-04-29 12:02:05"));
        meetingVo.setEndTime(sdf.parse("2020-04-29 12:02:05"));
        meetingVo.setReserveTime(new Date());
        meetingVo.setReserveId(8);
        meetingVo.setNumberOfPerson(20);
        meetingVo.setRoomId(7);
        meetingVo.setDesc("ceshi");
        meetingVo.setStatus("0");

        Employee e1 = new Employee();
        e1.setId(12);

        Employee e2 = new Employee();
        e2.setId(13);

        meetingVo.setEmployeeList(Arrays.asList(e1, e2));

        int result = meetingService.addMeeting(meetingVo);
        if(result==0){
            System.out.println("预定会议失败");

        }

    }



}