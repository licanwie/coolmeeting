<!DOCTYPE html>
<html>
<head>
    <title>CoolMeeting会议管理系统</title>
    <link rel="stylesheet" href="/static/css/common.css"/>
    <script src="/static/js/jquery3.5.1.js"></script>
</head>
<body>

<#include '../top.ftl'>

<div class="page-body">

    <#include '../leftMenu.ftl'>

    <div class="page-content">
        <div class="content-nav">
            人员管理 ><a href="/admin/depts">部门管理</a>
        </div>
        <form action="/admin/addDept" method="post">
            <fieldset>
                <legend>添加部门</legend>
                部门名称:
                <input type="text" required name="name" maxlength="20"/>
                <input type="submit" class="clickbutton" value="添加"/>

                <#if error??>
                    <span style="color: red">${error!''}</span>
                </#if>

            </fieldset>
        </form>

        <table class="listtable">
            <caption>所有部门:</caption>
            <tr class="listheader">
                <th>部门编号</th>
                <th>部门名称</th>
                <th>操作</th>
            </tr>

            <#if depts??>
                <#list depts as dept>
                    <tr>
                        <td>${dept.id}</td>
                        <td id="deptName${dept.id}">${dept.name}</td>
                        <td>
                            <a class="clickbutton" href="#" id="edit${dept.id}"
                               onclick="editDept(${dept.id})">编辑</a>
                            <a class="clickbutton" style="display: none" href="#" id="cancel${dept.id}"
                               onclick="cancelDept(${dept.id})">取消</a>
                            <a class="clickbutton" href="/admin/deleteDept/${dept.id}">删除</a>
                        </td>
                    </tr>
                </#list>
            </#if>
        </table>
    </div>
</div>

<#include '../footer.ftl'>

<script>

    var deptNameInput;

    function editDept(depId) {
        var editBtn = $('#edit' + depId);
        var cancelBtn = $('#cancel' + depId);
        var ele = $('#deptName' + depId);
        deptNameInput = ele.html();


        if (cancelBtn.css('display') == 'none') {
            cancelBtn.css('display', 'inline');
            editBtn.html('确定');
            var deptName = ele.text();
            ele.html('<input type="text" required value="' + deptName + '" />')
        }else{
            var children = ele.children('input');
            var newDeptName = children.val();

            var oldDeptName = $(deptNameInput).get(0).value

            if(newDeptName.trim()==''){
                alert('部门名称不可为空');
                ele.html(oldDeptName)
                cancelBtn.css('display', 'none');
                editBtn.html('编辑');
                return;
            }

            var oldDeptName = $(deptNameInput).get(0).value

            // 未改名情况
            if(newDeptName==oldDeptName){
                ele.html(oldDeptName)
                cancelBtn.css('display', 'none');
                editBtn.html('编辑');
                return;
            }

            $.post('/admin/updateDept',{id:depId, name:newDeptName},function (msg) {
                if (msg == 'success') {
                    cancelBtn.css('display', 'none');
                    editBtn.html('编辑');
                    ele.html(newDeptName);
                }
                else {
                    // 更新失败
                    alert(msg);
                    ele.html(oldDeptName)
                    cancelBtn.css('display', 'none');
                    editBtn.html('编辑');

                }
            })
        }
    }

    function cancelDept(depId) {
        var editBtn = $('#edit' + depId);
        var cancelBtn = $('#cancel' + depId);
        var ele = $('#deptName' + depId);
        cancelBtn.css('display', 'none');
        editBtn.html('编辑');
        ele.html(deptNameInput);
    }

</script>
</body>
</html>