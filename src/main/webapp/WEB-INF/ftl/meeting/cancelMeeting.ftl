<!DOCTYPE html>
<html>
    <head>
        <title>CoolMeeting会议管理系统</title>
        <link rel="stylesheet" href="/static/css/common.css"/>
    </head>
    <body>

    <#include '../top.ftl'>

        <div class="page-body">

            <#include '../leftMenu.ftl'>

            <div class="page-content">
                <div class="content-nav">
                    会议预定 ><a href="/cancelMeeting/${meetingVo.id}">取消会议预定</a>
                </div>
                <form action="/doCancelMeeting" method="post">
                    <fieldset>
                        <legend>取消预定</legend>
                        <table class="formtable">
                            <tr>
                                <td>会议名称：</td>
                                <td>${meetingVo.name!''}</td>
                            </tr>
                            <tr>
                                <td>会议室名称：</td>
                                <td>${meetingVo.roomName!''}</td>
                            </tr>
                            <tr>
                                <td>预计开始时间：</td>
                                <td>${meetingVo.startTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                            </tr>
                            <tr>
                                <td>预计结束时间：</td>
                                <td>${meetingVo.endTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                            </tr>
                            <tr>
                                <td>会议预定时间：</td>
                                <td>${meetingVo.reserveTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                            </tr>
                            <tr>
                                <td>会议预定人：</td>
                                <td>${meetingVo.reserveName}</td>
                            </tr>
                            <tr>
                                <td>取消理由：</td>
                                <td><textarea name="cancelReason" required rows="5"></textarea></td>
                            </tr>
                            <tr>
                                <td class="command" colspan="2">
                                    <input type="hidden" name="id" value="${meetingVo.id}">
                                    <input type="submit" class="clickbutton" value="确认取消"/>
                                    <input type="button" class="clickbutton" value="返回" onclick="window.history.back();"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
        </div>

    <#include '../footer.ftl'>

    </body>
</html>