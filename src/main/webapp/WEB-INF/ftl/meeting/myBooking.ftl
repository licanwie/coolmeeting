<!DOCTYPE html>
<html>
    <head>
        <title>CoolMeeting会议管理系统</title>
        <link rel="stylesheet" href="/static/css/common.css"/>
    </head>
    <body>

    <#include '../top.ftl'>

        <div class="page-body">

            <#include '../leftMenu.ftl'>

            <div class="page-content">
                <div class="content-nav">
                    个人中心 ><a href="/myBooking">我的预定</a>
                </div>
                <table class="listtable">
                    <caption>我预定的会议：</caption>

                    <tr class="listheader">
                        <th>会议名称</th>
                        <th>会议状态</th>
                        <th>会议室名称</th>
                        <th>会议开始时间</th>
                        <th>会议结束时间</th>
                        <th>会议预定时间</th>
                        <th>操作</th>
                    </tr>

                    <#if meetingVos??>
                        <#list meetingVos as meetingVo>
                            <tr>
                                <td>${meetingVo.name}</td>
                                <td style="color:${(meetingVo.status=='0')?string('green','red')}">
                                    ${(meetingVo.status=="0")?string("正常","已取消")}
                                </td>
                                <td>${meetingVo.roomName}</td>
                                <td>${meetingVo.startTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>${meetingVo.endTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>${meetingVo.reserveTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td style="float:left">
                                    <a class="clickbutton" href="/meetingDetail/${meetingVo.id}">查看</a>
                                    <#if meetingVo.status=="0">
                                        <a class="clickbutton" href="/cancelMeeting/${meetingVo.id}">取消</a>
                                    </#if>
                                </td>
                            </tr>
                        </#list>
                    </#if>

                </table>
            </div>
        </div>

    <#include '../footer.ftl'>

    </body>
</html>
