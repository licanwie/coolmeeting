<!DOCTYPE html>
<html>
    <head>
        <title>CoolMeeting会议管理系统</title>
        <link rel="stylesheet" href="/static/css/common.css"/>
    </head>
    <body>

    <#include '../top.ftl'>

        <div class="page-body">

            <#include '../leftMenu.ftl'>

            <div class="page-content">
                <div class="content-nav">
                    会议预定 ><a href="/meetingDetail/${meetingVo.id}">会议详情</a>
                </div>
                <form>
                    <fieldset>
                        <legend>会议信息</legend>
                        <table class="formtable">
                            <tr>
                                <td>会议名称：</td>
                                <td>${meetingVo.name!''}</td>
                            </tr>
                            <tr>
                                <td>会议状态：</td>
                                <td style="color:${(meetingVo.status=='0')?string('green','red')}">
                                    ${(meetingVo.status=="0")?string("正常","已取消")}
                                </td>
                            </tr>
                            <tr>
                                <td>会议室名称：</td>
                                <td>${meetingVo.roomName!''}</td>
                            </tr>
                            <tr>
                                <td>预计参加人数：</td>
                                <td>${meetingVo.employeeList?size}</td>
                            </tr>
                            <tr>
                                <td>预计开始时间：</td>
                                <td>${meetingVo.startTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                            </tr>
                            <tr>
                                <td>预计结束时间：</td>
                                <td>${meetingVo.endTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                            </tr>
                            <tr>
                                <td>预定时间：</td>
                                <td>${meetingVo.reserveTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                            </tr>
                            <tr>
                                <td>预定者：</td>
                                <td>${meetingVo.reserveName}</td>
                            </tr>
                            <tr>
                                <td>会议说明：</td>
                                <td>
                                    <textarea name="desc" rows="5" readonly>${meetingVo.desc!''}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>参会人员：</td>
                                <td>
                                    <table class="listtable">

                                        <tr class="listheader">
                                            <th>姓名</th>
                                            <th>联系电话</th>
                                            <td>电子邮件</td>
                                        </tr>

                                        <#if meetingVo??>
                                            <#list meetingVo.employeeList as emp>
                                                <tr>
                                                    <td>${emp.realName}</td>
                                                    <td>${emp.phone}</td>
                                                    <td>${emp.email}</td>
                                                </tr>
                                            </#list>
                                        </#if>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="command" colspan="2">
                                    <input type="button" class="clickbutton" value="返回" onclick="window.history.back();"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
        </div>

    <#include '../footer.ftl'>

    </body>
</html>