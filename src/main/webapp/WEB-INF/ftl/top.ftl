<div class="page-header">
    <div class="header-banner">
        <img src="/static/img/header.png" alt="CoolMeeting"/>
    </div>
    <div class="header-title">
        欢迎访问Cool-Meeting会议管理系统
    </div>
    <div class="header-quicklink">
        欢迎您，
        <#if user??>
            <span>${user.realName!''}</span>
            <a href="/changePassword">[修改密码] </a>
        <#else>
            <strong>陌生人</strong>
        </#if>
        <#if user??>
            <a href="/logOut">退出</a>
        </#if>
    </div>
</div>