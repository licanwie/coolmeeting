<!DOCTYPE html>
<html>
<head>
    <title>CoolMeeting会议管理系统</title>
    <link rel="stylesheet" href="/static/css/common.css"/>
</head>
<body>

<#include '../top.ftl'>

<div class="page-body">

    <#include '../leftMenu.ftl'>

    <div class="page-content">
        <div class="content-nav">
            会议预定 ><a href="/meetingRoom">查看会议室</a>
        </div>
        <table class="listtable">
            <caption>所有会议室:</caption>
            <tr class="listheader">
                <th>门牌编号</th>
                <th>会议室名称</th>
                <th>容纳人数</th>
                <th>当前状态</th>
                <th>操作</th>
            </tr>
            <#if rooms??>
                <#list rooms as room>
                    <tr>
                        <td>${room.roomNum}</td>
                        <td>${room.name}</td>
                        <td>${room.capacity}</td>
                        <td style="color:${(room.status=="0")?string('green','red')}">${(room.status=="0")?string("可使用","已停用")}</td>
                        <td>
                            <a class="clickbutton" href="/roomDetail/${room.id}">查看详情</a>
                        </td>
                    </tr>
                </#list>
            </#if>
        </table>
    </div>
</div>

<#include '../footer.ftl'>

</body>
</html>
