<!DOCTYPE html>
<html>
<head>
    <title>CoolMeeting会议管理系统</title>
    <link rel="stylesheet" href="/static/css/common.css"/>
</head>
<body>

<#include 'top.ftl'>

<div class="page-body">

    <#include 'leftMenu.ftl'>

    <div class="page-content">
        <div class="content-nav">
            人员管理 ><a href="/register">员工注册</a>
        </div>
        <form action="/doRegister" method="post">
            <fieldset>
                <legend>员工信息</legend>
                <div style="color: #ff0114">${error!''}</div>
                <table class="formtable" style="width:50%">
                    <tr>
                        <td>姓名：</td>
                        <td>
                            <input name="realName" type="text" required value="<#if employee??>${employee.realName}</#if>" maxlength="20"/>
                        </td>
                    </tr>
                    <tr>
                        <td>账户名：</td>
                        <td>
                            <input name="username" type="text" required value="<#if employee??>${employee.username}</#if>" maxlength="20"/>
                        </td>
                    </tr>
                    <tr>
                        <td>密码：</td>
                        <td>
                            <input name="password" type="password" required id="password" maxlength="20" placeholder="请输入6位以上的密码"/>
                        </td>
                    </tr>
                    <tr>
                        <td>确认密码：</td>
                        <td>
                            <input type="password" id="confirm" required maxlength="20" onchange="check()"/>
                            <div style="color: #ff0114" id="confirmInfo"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>联系电话：</td>
                        <td>
                            <input name="phone" type="text" value="<#if employee??>${employee.phone}</#if>" maxlength="20"/>
                        </td>
                    </tr>
                    <tr>
                        <td>电子邮件：</td>
                        <td>
                            <input name="email" type="text" value="<#if employee??>${employee.email}</#if>" maxlength="20"/>
                        </td>
                    </tr>
                    <td>所在部门：</td>
                    <td>
                        <select name="deptId">
                            <#if depts??>
                                <#list depts as dept>
                                    <option value="${dept.id}">${dept.name}</option>
                                </#list>
                            </#if>
                        </select>
                    </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="command">
                            <input type="submit" class="clickbutton" value="注册"/>
                            <input type="reset" class="clickbutton" value="重置"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>

<#include 'footer.ftl'>

<script>
    // 前端检查密码是否一致
    function check() {
        var password = document.getElementById('password');
        var confirm = document.getElementById('confirm');
        var confirmInfo = document.getElementById('confirmInfo');
        if (password.value !== confirm.value) {
            confirmInfo.innerHTML = '两次输入密码不一致';
            confirm.value="";
        }else{
            confirmInfo.innerHTML = '';
        }
    }
</script>
</body>
</html>