<!DOCTYPE html>
<html>
    <head>
        <title>CoolMeeting会议管理系统</title>
        <link rel="stylesheet" href="/static/css/common.css"/>
    </head>
    <body>

    <#include 'top.ftl'>

        <div class="page-body">

            <#include 'leftMenu.ftl'>

            <div class="page-content">
                <div class="content-nav">
                    修改密码
                </div>
                <form action="/doChangePassword" method="post">
                    <fieldset>
                        <legend>修改密码信息</legend>
                        <table class="formtable" style="width:50%">
                            <tr>
                                <td>原密码:</td>
                                <td>
                                    <input name="oldPassword" required type="password" />
                                </td>
                            </tr>
                            <tr>
                                <td>新密码:</td>
                                <td>
                                    <input id="newPassword" name="newPassword" required type="password" />
                                </td>
                            </tr>
                            <tr>
                                <td>确认新密码：</td>
                                <td>
                                    <input id="confirm" name="confirm" required type="password" onchange="check()"/>
                                    <div style="color: #ff0114" id="confirmInfo">${error!''}</div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="command">
                                    <input type="submit" value="确认修改" class="clickbutton"/>
                                    <input type="button" value="返回" class="clickbutton" onclick="window.history.back();"/>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
            </div>
        </div>

    <#include 'footer.ftl'>

    <script>
        // 前端检查密码是否一致
        function check() {
            var password = document.getElementById('newPassword');
            var confirm = document.getElementById('confirm');
            var confirmInfo = document.getElementById('confirmInfo');
            if (password.value !== confirm.value) {
                confirmInfo.innerHTML = '两次输入密码不一致';
                confirm.value="";
            }else{
                confirmInfo.innerHTML = '';
            }
        }
    </script>

    </body>
</html>
