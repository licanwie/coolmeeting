﻿<!DOCTYPE html>
<html>
<head>
    <title>CoolMeeting会议管理系统</title>
    <link rel="stylesheet" href="/static/css/common.css"/>
</head>
<body>

<#include 'top.ftl'>

<div class="page-body">

    <#include 'leftMenu.ftl'>

    <div class="page-content">
        <div class="content-nav">
            个人中心 ><a href="/notice">最新通知</a>
        </div>
        <table class="listtable">
            <caption>
                我要参加的会议:
            </caption>
            <tr class="listheader">
                <th>会议名称</th>
                <th>会议室</th>
                <th>起始时间</th>
                <th>结束时间</th>
                <th>预订者</th>
                <th>操作</th>
            </tr>

            <#if normalMeetings??>
                <#list normalMeetings as nm>
                    <tr>
                        <td>${nm.name}</td>
                        <td>${nm.roomName}</td>
                        <td>${nm.startTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                        <td>${nm.endTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                        <td>${nm.reserveName}</td>
                        <td>
                            <a class="clickbutton" href="/meetingDetail/${nm.id}">查看详情</a>
                        </td>
                    </tr>
                </#list>
            </#if>

        </table>
        <table class="listtable">
            <caption>
                已取消的会议:
            </caption>
            <tr>
                <th style="width:150px">会议名称</th>
                <th>会议室</th>
                <th>起始时间</th>
                <th>结束时间</th>
                <th>预订者</th>
                <th>取消原因</th>
                <th>操作</th>
            </tr>

            <#if cancelMeetings??>
                <#list cancelMeetings as cm>
                    <tr>
                        <td>${cm.name}</td>
                        <td>${cm.roomName}</td>
                        <td>${cm.startTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                        <td>${cm.endTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                        <td>${cm.reserveName}</td>
                        <td style="float:left">${(cm.cancelReason?trim)}</td>
                        <td>
                            <a class="clickbutton" href="/meetingDetail/${cm.id}">查看详情</a>
                        </td>
                    </tr>
                </#list>
            </#if>

        </table>

    </div>

</div>

<#include 'footer.ftl'>

</body>
</html>