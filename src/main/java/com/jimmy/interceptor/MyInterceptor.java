package com.jimmy.interceptor;

import com.jimmy.pojo.Employee;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.nio.file.PathMatcher;
import java.util.Arrays;
import java.util.List;

/**
 * 自定义拦截器
 * 登录认证 权限认证
 */

public class MyInterceptor implements HandlerInterceptor {
    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        // 登录，注册页面不拦截
        List<String> list = Arrays.asList("login", "doLogin", "register", "doRegister");

        for (String str : list) {
            if(uri.contains(str)){
                return true;
            }
        }

        HttpSession session = request.getSession(true);
        Employee user = (Employee)session.getAttribute("user");
        if(antPathMatcher.match("/admin/**", uri)){
            if(user!=null){
                if("2".equals(user.getRole())){ // 的确是管理员
                    return true;
                }
                else {
                    response.setHeader("Content-type", "text/html;charset=utf-8");
                    response.setCharacterEncoding("utf-8");
                    response.getWriter().write("forbidden 没有权限！");
                    return false;
                }
            }
        }else if(user!=null){// 已登录 且 路径非管理员路径
            return true;
        }
        response.sendRedirect("/login"); // 重定向到登录页面
        return false;

    }
}
