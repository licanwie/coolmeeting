package com.jimmy.service;

import com.jimmy.pojo.Room;

import java.util.List;

public interface RoomService {

    // 获取所有房间
    List<Room> getAllRoom();

    // 获取单个房间
    Room getRoomById(int id);

    // 更新房间
    int updateRoom(Room room);

    // 新增房间
    int addRoom(Room room);

}
