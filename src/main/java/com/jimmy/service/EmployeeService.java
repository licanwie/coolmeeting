package com.jimmy.service;

import com.jimmy.pojo.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeService {

    // 用户登录
    Employee loginEmployee(String username, String password);

    // 用户注册
    int registerEmployee(Employee employeee);

    // 获取用户
    Employee getEmployeeByUsername(String username);

    // 根据状态获取员工
    List<Employee> getEmployeeByStatus(String status);

    // 账号审核，通过还是不通过
    int updateEmpStatus(int id, String status);

    // 分页获取员工
    List<Employee> getEmployeeBySpecification(Employee employee, int page, int pageSize);

    // 统计员工数量
    int countEmpSizeBySpecification(Employee employee);

    // 获取某部门下员工
    List<Employee> getEmpByDeptId(int deptId);

    // 修改密码
    int updateEmpPwdById(int id, String password);

    // 根据id获取员工
    Employee getEmployeeById(int id);

    // 获取会议下员工
    List<Employee> getEmployeeByMeetingId(int meetingId);

    // 修改员工所属部门
    int updateEmpDeptIdByDeptId(int deptId, int newDeptId);

}
