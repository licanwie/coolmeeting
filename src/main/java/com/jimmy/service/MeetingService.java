package com.jimmy.service;

import com.jimmy.pojo.Meeting;
import com.jimmy.pojo.vo.MeetingVo;

import java.util.List;
import java.util.Map;

public interface MeetingService {
    // 根据 预订者 获取会议
    List<MeetingVo> getMeetingByReserveId(int reserveId);

    // 获取某员工要参加的所有会议
    List<MeetingVo> getMeetingByEmpId(int empId);

    // 获取单个会议
    MeetingVo getMeetingById(int id);

    // 取消会议
    int cancelMeetingById(int meetingId, String cancelReason);

    // 根据指定条件获取会议
    List<MeetingVo> getMeetingByMap(Map<String, Object> map);

    // 新增会议
    int addMeeting(MeetingVo meetingVo);

}
