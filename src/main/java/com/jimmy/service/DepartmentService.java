package com.jimmy.service;

import com.jimmy.pojo.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DepartmentService {

    // 根据名称获取部门
    Department getDeptByName(String name);

    // 获取所有部门
    List<Department> getAllDepts();

    // 新增部门
    int addDept(String name);

    // 删除部门
    int deleteDeptById(int id);

    // 更新部门
    int updateDeptById(int id, String deptName);

}
