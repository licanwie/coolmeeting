package com.jimmy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 会议实体类
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Meeting implements Serializable {
    private int id;
    private String name;
    private int roomId;
    private int reserveId;// 预定人id
    private int numberOfPerson;
    private Date startTime;
    private Date endTime;
    private Date reserveTime;
    private String desc;
    private String status;
    private Date cancelTime;
    private String cancelReason;

}
