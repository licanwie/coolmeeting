package com.jimmy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 房间实体类
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Room implements Serializable {
    private int id;
    private int roomNum; // 房间编号
    private String name;
    private int capacity;
    private String status;
    private String desc;

}
