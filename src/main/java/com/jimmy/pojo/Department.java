package com.jimmy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 部门实体类
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department implements Serializable {
    private int id;
    private String name;

}
