package com.jimmy.pojo.vo;

import com.jimmy.pojo.Employee;
import com.jimmy.pojo.Meeting;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 向前端传输数据用的viewModel类
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class MeetingVo extends Meeting {

    private String roomName;// 会议房间名

    private String reserveName;// 会议预订者realName

    private List<Employee> employeeList;// 参会的员工列表

}
