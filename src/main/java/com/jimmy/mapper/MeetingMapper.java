package com.jimmy.mapper;

import com.jimmy.pojo.Meeting;
import com.jimmy.pojo.vo.MeetingVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MeetingMapper {

    // 根据会议预订者id 获取会议
    List<MeetingVo> getMeetingByReserveId(@Param("reserveId")int reserveId);

    // 根据员工id获取要参加的会议
    List<MeetingVo> getMeetingByEmpId(@Param("empId")int empId);

    MeetingVo getMeetingById(@Param("id")int meetingId);

    int cancelMeetingById(@Param("id")int meetingId, @Param("reason")String cancelReason);

    List<MeetingVo> getMeetingByMap(Map<String, Object> map);

    int addMeeting(MeetingVo meetingVo);

    int addEmpToMeeting(MeetingVo meetingVo);

}
