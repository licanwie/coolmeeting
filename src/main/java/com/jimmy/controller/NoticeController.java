package com.jimmy.controller;

import com.jimmy.pojo.Employee;
import com.jimmy.pojo.vo.MeetingVo;
import com.jimmy.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class NoticeController {
    @Autowired
    private MeetingService meetingService;

    // 首页，通知页，展示员工有关的会议

    @GetMapping({"/", "/notice", "/index", "/index.html"})
    public String notice(Model model, HttpSession session){
        Employee emp = (Employee)session.getAttribute("user");
        int empId = emp.getId();

        List<MeetingVo> meetingList = meetingService.getMeetingByEmpId(empId);
        // 这里简单把与用户相关的会议分为 没取消的和已取消的，会议时间有效性问题不考虑
        List<MeetingVo> normalMeetings = new ArrayList<>();
        List<MeetingVo> cancelMeetings = new ArrayList<>();

        for (MeetingVo meetingVo : meetingList) {
            if("0".equals(meetingVo.getStatus())){
                normalMeetings.add(meetingVo);
            }
            else {
                cancelMeetings.add(meetingVo);
            }
        }
        System.out.println(normalMeetings);
        System.out.println(cancelMeetings);

        model.addAttribute("normalMeetings", normalMeetings);
        model.addAttribute("cancelMeetings", cancelMeetings);
        return "notice";

    }

}
