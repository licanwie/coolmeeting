package com.jimmy.controller;

import com.jimmy.pojo.Department;
import com.jimmy.service.DepartmentService;
import com.jimmy.service.EmployeeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/admin/depts")
    public String toDepartments(Model model){
        List<Department> depts = departmentService.getAllDepts();
        model.addAttribute("depts", depts);

        return "admin/departments";
    }

    // 响应前端 ajax请求，返回所有部门数据

    @RequestMapping("/getAllDepts")
    @ResponseBody
    public List<Department> getAllDepts(){
        List<Department> depts = departmentService.getAllDepts();
        return depts;
    }

    // 新增部门

    @RequestMapping("/admin/addDept")
    public String addDept(String name, Model model){
        if(StringUtils.isBlank(name)){
            model.addAttribute("error", "部门名称不可为空");
            return "forward:/admin/depts";
        }

        Department dept = departmentService.getDeptByName(name);
        if(dept!=null){
            model.addAttribute("error", "部门名称重复");
            return "forward:/admin/depts";
        }

        int i = departmentService.addDept(name);
        if(i!=1){
            model.addAttribute("error", "新增部门失败");
            return "forward:/admin/depts";
        }
        return "redirect:/admin/depts";
    }

    // 删除部门

    @RequestMapping("/admin/deleteDept/{id}")
    public String deleteDept(@PathVariable("id")int id, Model model){
        // 删除的部门下 已有员工 部门id属性设为0
        int i = employeeService.updateEmpDeptIdByDeptId(id, 0);
        System.out.println("部门下有"+i+"位员工部门id属性设为0");

        int result = departmentService.deleteDeptById(id);
        if(result!=1){
            System.out.println("\n\n=====删除部门失败======\n\n");
            model.addAttribute("error", "删除部门失败");
            return "forward:/admin/depts";
        }
        return "redirect:/admin/depts";
    }

    // 响应前端 ajax请求，修改部门数据

    @RequestMapping(value = "/admin/updateDept", produces = {"text/html;charset=utf-8;"})
    @ResponseBody
    public String updateDept(int id, String name){
        Department dept = departmentService.getDeptByName(name);
        if(dept!=null){
            System.out.println("\n\n=====部门名称重复======\n\n");
            return "部门名称重复";
        }

        int result = departmentService.updateDeptById(id, name);
        if(result!=1){
            System.out.println("\n\n=====更新部门名称失败======\n\n");
            return "更新部门名称失败";
        }
        return "success";

    }

}
