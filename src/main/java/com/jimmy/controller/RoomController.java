package com.jimmy.controller;

import com.jimmy.pojo.Room;
import com.jimmy.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class RoomController {
    @Autowired
    private RoomService roomService;

    // 展示所有房间

    @RequestMapping("/meetingRoom")
    public String toMeetingRoom(Model model){
        List<Room> rooms = roomService.getAllRoom();

        model.addAttribute("rooms", rooms);
        return "room/meetingRoom";
    }

    // 房间详情

    @GetMapping("/roomDetail/{id}")
    public String roomDetail(@PathVariable("id")int id, Model model){
        Room room = roomService.getRoomById(id);
        System.out.println(room);
        model.addAttribute("room", room);
        return "room/roomDetail";

    }

    // 修改房间

    @PostMapping("/updateRoom")
    public String updateRoom(Room room){
        System.out.println(room);
        int result = roomService.updateRoom(room);
        if(result==1){
            return "redirect:/meetingRoom"; // 更新成功
        }
        else{
            return "forward:/roomDetail/"+room.getId(); // 更新失败
        }

    }

    // 新增房间

    @RequestMapping("/admin/addMeetingRoom")
    public String toAddMeetingRoom(){
        return "admin/addMeetingRoom";

    }

    @RequestMapping("/admin/doAddMr")
    public String doAddMr(Room room){
        System.out.println(room);
        int result = roomService.addRoom(room);
        if(result==1){
            return "redirect:/meetingRoom"; // 添加成功
        }
        else{
            return "forward:/admin/doAddMr";// 添加失败
        }


    }

}
