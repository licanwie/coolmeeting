package com.jimmy.controller;

import com.jimmy.pojo.Department;
import com.jimmy.pojo.Employee;
import com.jimmy.service.DepartmentService;
import com.jimmy.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * forward和redirect是servlet种的两种主要的跳转方式 forward叫转发 redirect叫做重定向
 * forword是服务器内部的重定向，服务器直接访问目标地址的 url网址，把里面的东西读取出来，但是客户端并不知道，因此用forward的话，客户端浏览器的网址不发生变化
 * redirect是服务器根据逻辑，发送一个状态码，告诉浏览器重新去请求那个地址，所以地址栏显示的是新的地址。
 * 由于在整个定向的过程中用的是同一个request，因此forward会将request的信息带到被重定向的jsp或者servlet中使用。即可以共享数据
 * redirect不能共享
 *
 * forword转发是服务器上的行为  redirect重定向是客户端的行为
 * forword只有一次请求 redirect有两次请求
 *
 */

@Controller
public class LoginController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private DepartmentService departmentService;

    // 登录

    @RequestMapping({"login", "login.html"})
    public String toLogin(){
        return "login";
    }

    @PostMapping("/doLogin")
    public String doLogin(String username, String password, Model model, HttpSession httpSession){
        System.out.println(username+password);

        Employee employee = employeeService.loginEmployee(username, password);
        if (employee == null){
            model.addAttribute("error", "用户名或密码错误");
            return "forward:/login"; // 这里用 forward 不能用 redirect。 获取login页面，把model塞进去 浏览器的网址不发生变化
        }else if("1".equals(employee.getStatus())){
            model.addAttribute("error", "用户未审批");
            return "forward:/login";
        }else if("2".equals(employee.getStatus())){
            model.addAttribute("error", "用户审批未通过");
            return "forward:/login";
        }else {
            httpSession.setAttribute("user", employee);
            return "redirect:/notice";
        }

    }

    // 退出登录

    @RequestMapping("/logOut")
    public String logOut(HttpSession httpSession){
        httpSession.setAttribute("user", null);
        return "redirect:/login";
    }

    // 注册

    @RequestMapping("/register")
    public String toRegister(Model model){
        List<Department> depts = departmentService.getAllDepts();
        System.out.println(depts);

        model.addAttribute("depts", depts);
        return "register";
    }

    @PostMapping("/doRegister")
    public String doRegister(Employee employee, Model model, HttpSession session){
        System.out.println(employee);
        int i = employeeService.registerEmployee(employee);
        if(i==1){
            session.setAttribute("user", null);
            return "redirect:/login"; // 注册成功，重定向到登录页面
        }else {
            model.addAttribute("error", "注册失败");
            model.addAttribute("employee", employee);// 数据自动赋值
            return "forward:/register"; // 注册失败，重新注册
        }

    }

    // 修改密码

    @RequestMapping("/changePassword")
    public String toChangePassword(){
        return "changePassword";
    }

    @RequestMapping("/doChangePassword")
    public String doChangePassword(String oldPassword, String newPassword, Model model, HttpSession httpSession){
        System.out.println(oldPassword);
        System.out.println(newPassword);

        Employee e = (Employee) httpSession.getAttribute("user");
        String username = e.getUsername();
        int empId = e.getId();
        // 验证原密码是否正确
        Employee emp = employeeService.loginEmployee(username, oldPassword);
        if(emp==null){
            model.addAttribute("error", "原密码校验失败");
            return "forward:/changePassword";// 验证密码失败
        }

        int result = employeeService.updateEmpPwdById(empId, newPassword);
        if(result!=1){
            model.addAttribute("error", "密码修改失败");
            return "forward:/changePassword";// 修改密码失败
        }
        return "redirect:/login";// 修改密码成功，重新登录

    }

}
