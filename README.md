# coolmeeting

[![Spring](https://img.shields.io/badge/Spring-v5.2.7-brightgreen)]() [![SpringMVC](https://img.shields.io/badge/SpringMVC-v5.2.7-brightgreen) ![MyBatis](https://img.shields.io/badge/MyBatis-v3.5.2-brightgreen)]() [![FreeMarker](https://img.shields.io/badge/FreeMarker-v2.3.30-brightgreen)]() [![MySql](https://img.shields.io/badge/MySql-v5.7-brightgreen)]() [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

coolmeeting是一个SSM框架整合的小项目，前端采用了FreeMarker模板引擎。

## 内容列表

- [背景](#背景)
- [安装](#安装)
- [环境](#环境)
- [在线预览](#在线预览)
- [相关项目](#相关项目)
- [维护者](#维护者)
- [如何贡献](#如何贡献)
- [开源协议](#开源协议)

## 背景
### coolmeeting介绍

coolmeeting是一个**SSM**整合开发的会议管理系统，前端方面使用了**FreeMarker**模板引擎，使用**MySql**存放数据，系统本身并不复杂，适合于初学者学习并巩固用SSM框架搭建项目。

coolmeeting项目代码原托管地址：https://github.com/lenve/CoolMeeting

coolmeeting B站视频教程地址：https://www.bilibili.com/video/BV1ep4y1S7s6

需要说明的是，原代码功能不完善，且数据库设计，前后端代码多有不合理之处，故本人在原代码的基础上根据自己的理解设计修改了部分逻辑。

如果觉得本项目对您有帮助的话，不妨为我点一颗右上方的小 <a><img width="3%" src="img/star.png"/></a> ，不胜感激！

### 项目截图

**登录**：正常状态下的用户可以登录。包括普通用户和管理员。

![登录](img/login.png)

**修改密码**：用户可以修改自己的登录密码。

![修改密码](img/changePwd.png)

**通知**：登录后显示的主页。展示相关的会议信息。

![通知](img/notice.png)

**我的预定**：展示由用户本人预定的会议。

![我的预定](img/myBooking.png)

**我的会议**：展示用户本人需要参加的会议。

![我的会议](img/myMeeting.png)

**会议详情**：展示会议的具体信息。

![会议详情](img/meetingDetail.png)

**员工注册**：员工注册页面，注册的用户还需管理员审批通过后才能登陆。

![员工注册](img/register.png)

**部门管理**：部门管理页面，包括新增，修改，删除部门。

![部门管理](img/department.png)

**注册审批**：管理员对待审批的用户进行审批操作，通过或不通过。

![注册审批](img/approve.png)

**搜索员工**：按照姓名，账号名，状态搜索员工。

![搜索员工](img/searchEmp.png)

**添加会议室**：添加会议室。

![添加会议室](img/addRoom.png)

**查看会议室**：查看所有的会议室信息。

![查看会议室](img/allRoom.png)

**会议室详情**：查看，修改会议室详细信息。

![会议室详情](img/roomDetail.png)

**预定会议**：预定会议页面。

![预定会议](img/bookMeeting.png)

**搜索会议**：按照条件搜索会议。

![搜索会议](img/searchMeeting.png)

------

## 安装

无需安装

## 环境
* Spring v5.2.7
* SpringMVC v5.2.7
* MyBatis v3.5.2
* FreeMarker v2.3.30
* MySql v5.7

## 在线预览

暂无

## 相关项目

coolmeeting项目代码原托管地址：https://github.com/lenve/CoolMeeting

coolmeeting B站视频教程地址：https://www.bilibili.com/video/BV1ep4y1S7s6

## 维护者

[@JimmyDug](https://gitee.com/jimmydug)。

## 如何贡献

[![](https://img.shields.io/badge/%E7%94%B3%E8%AF%B7-Pull%20Request-orange)](https://gitee.com/jimmydug/coolmeeting/pulls)

### 贡献者
<a href='https://gitee.com/jimmydug'><img width="8%" src="https://portrait.gitee.com/uploads/avatars/user/2546/7640829_jimmydug_1591244634.png!avatar200" /></a>


## 开源协议

[<img src="https://img.shields.io/badge/license-MIT-green">](LICENSE) © JimmyDug